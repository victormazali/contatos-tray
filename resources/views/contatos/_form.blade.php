            <div class="form-body">
                <div class="alert alert-danger display-hide">
                <button class="close" data-close="alert"></button>
                Você tem alguns erros de formulário. Por favor, verifique abaixo.
                </div>
                <div class="alert alert-success display-hide">
                <button class="close" data-close="alert"></button> Sua validação de formulário foi bem sucedida! </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Nome
                        <span class="required"> * </span></label>
                        <div class="col-md-5">
                            <div class="input-icon right">
                                <i class="fa fa-user"></i>
                                <input name="name" type="text" class="form-control" placeholder="Enter name" value="{{ old('name') ? old('name') : $contato->name }}"> </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Email
                        <span class="required"> * </span></label>
                        <div class="col-md-5">
                            <div class="input-icon right">
                                <i class="fa fa-envelope"></i>
                                <input name="email" type="text" class="form-control" placeholder="Email Address" value="{{ old('email') ? old('email') : $contato->email }}"> </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Facebook
                        </label>
                        <div class="col-md-5">
                            <div class="input-icon right">
                                <i class="fa fa-facebook"></i>
                                <input name="facebook_link" type="text" class="form-control" placeholder="Enter facebook link" value="{{ old('name') ? old('name') : $contato->facebook_link }}"> </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">LinkedIn
                        </label>
                        <div class="col-md-5">
                            <div class="input-icon right">
                                <i class="fa fa-linkedin"></i>
                                <input name="linkedin_link" type="text" class="form-control" placeholder="Enter LinkedIn link" value="{{ old('name') ? old('name') : $contato->linkedin_link }}"> </div>
                        </div>
                    </div>
            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-3 col-md-9">
                        <button type="submit" class="btn blue-sharp">Salvar</button>
                        <a class="btn default" href="{{ route('contatos.index') }}">Cancelar</a>
                    </div>
                </div>
            </div>