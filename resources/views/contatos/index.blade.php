@extends('layouts.default')

@section('css-plugins')
        <!--Datatable-->
        <link href="{{ asset("assets/global/plugins/datatables/datatables.min.css") }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset("assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css") }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset("assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css") }}" rel="stylesheet" type="text/css" />
@endsection

@section('breadcumb')
	<li>
            Contatos
	</li>
@endsection

@section('content')
	
    <h1 class="page-title">
    	Contatos
        <small>Index</small>
    </h1>


		<a class="btn blue-sharp btn-outline sbold pull-right" href="{{ route('contatos.create') }}"><i class="fa fa-user-plus"></i> Novo Contato</a>
		<div style="margin:0; padding:0; height:50px;">&nbsp;</div>

    <div class="row">
                <div class="col-md-12">
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-dark">
                                            <i class="icon-settings font-dark"></i>
                                            <span class="caption-subject bold uppercase">Contatos</b></span>
                                        </div>
                                        <div class="tools"> </div>
                                    </div>
                                    <div class="portlet-body">
                                        <table class="table table-striped table-bordered table-hover" id="sample_1">
                                            <thead>
                                                <tr>
                                                    <th>Nome</th>
                                                    <th>Email</th>
                                                    <th>Facebook</th>
                                                    <th>LinkedIn</th>
                                                    <th>Ações</th>
                                                </tr>
                                            </thead>
                                        <tbody>
                                            @foreach($result as $r)
                                                <tr>
                                                    <td>{{$r->name}}</td>
                                                    <td>{{$r->email}}</td>
                                                    <td>{{$r->facebook_link}}</td>
                                                    <td>{{$r->linkedin_link}}</td>
                                                    <td>
                                                        <div class="btn-group">
                                                            <button class="btn btn-xs blue-sharp dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Ações
                                                                <i class="fa fa-angle-down"></i>
                                                            </button>
                                                            <ul class="dropdown-menu pull-left" role="menu">
                                                                <li>
                                                                    <a href="{{ route('contatos.edit', $r->id) }}">
                                                                        <i class="fa fa-edit"></i> Editar </a>
                                                                </li>
                                                                <li>
                                                                    <a href="{{ route('contatos.telefones', $r->id) }}">
                                                                        <i class="fa fa-phone"></i> Telefones </a>
                                                                </li>
                                                                <li>
                                                                <form role="form" action="{{ route('contatos.destroy', $r->id) }}" METHOD="POST">
                                                                    {{ csrf_field() }}
                                                                    {{ method_field('DELETE') }}<button type="submit">Deletar</button>
                                                                </form>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                </div>
    </div>
    
@endsection

@section('before-plugins')
        <!--Datatable-->
        <script src="{{ asset("assets/global/scripts/datatable.js") }}" type="text/javascript"></script>
        <script src="{{ asset("assets/global/plugins/datatables/datatables.min.js") }}" type="text/javascript"></script>
        <script src="{{ asset("assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js") }}" type="text/javascript"></script>
        <script src="{{ asset("assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js") }}" type="text/javascript"></script>
@endsection

@section('after-plugins')
        <!--Datatable-->
        <script src="{{ asset("assets/pages/scripts/table-datatables-buttons.min.js") }}" type="text/javascript"></script>

@endsection

@section('scripts')
 
@endsection