@extends('layouts.default')

@section('css-plugins')
        {{-- Select --}}
        <link href="{{ asset("assets/global/plugins/select2/css/select2.min.css") }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset("assets/global/plugins/select2/css/select2-bootstrap.min.css") }}" rel="stylesheet" type="text/css" />

        {{-- File Input --}}
        <link href="{{ asset("assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css") }}" rel="stylesheet" type="text/css" />
@endsection

@section('breadcumb')
	<li>
        <a href="{{ route('contatos.index') }}" class="nav-link nav-toggle">
            <span class="title">Contatos</span>
        </a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        Administrar telefones
    </li>
@endsection

@section('content')
	
    <h1 class="page-title">
    	Contatos
        <small>Administrar telefones</small>
    </h1>

    <a class="btn blue-sharp btn-outline sbold pull-right" data-toggle="modal" href="#telefone"><i class="fa fa-phone"></i> Novo Telefone</a>
    <div style="margin:0; padding:0; height:50px;">&nbsp;</div>

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-dark">
                <i class="icon-settings font-dark"></i>
                <span class="caption-subject bold uppercase">{{$contato->name}}</b></span>
            </div>
            <div class="tools"> </div>
        </div>
        <div class="portlet-body">
            <table class="table table-striped table-bordered table-hover" id="sample_1">
                <thead>
                    <tr>
                        <th>Numero</th>
                        <th>Deletar</th>
                    </tr>
                </thead>
            <tbody>
                @foreach($contato->telefones as $r)
                    <tr>
                        <td>{{$r->numero}}</td>
                        <td>
                            <form role="form" action="{{ route('telefones.destroy', $r->id) }}" METHOD="POST">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}<button type="submit">Deletar</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
                                

    {{-- Modal Adicionar Telefone --}}
    <div id="telefone" class="modal fade" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Adicionar Telefone</h4>
                </div>
                <form role="form" action="{{ route('telefones.store') }}" id="telefone-form" METHOD="POST">
                    <input type="hidden" name="contato_id" value="{{$contato->id}}">
                <div class="modal-body">
                    <div class="scroller" style="height:150px" data-always-visible="1" data-rail-visible1="1">
                        {{ csrf_field() }}
                        <div class="form-body">
                            <div class="form-group">
                                <label>Número<span class="required"> * </span></label>
                                <input type="text" class="form-control" name="numero" placeholder="Enter número">
                            </div>
                        </div> 
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn blue-sharp">Adicionar</button>
                </div>
                </form>
            </div>
        </div>
    </div>
    
@endsection

@section('before-plugins')
        {{-- Select --}}
        <script src="{{ asset("assets/global/plugins/select2/js/select2.full.min.js") }}" type="text/javascript"></script>

        {{-- Input File --}}
        <script src="{{ asset("assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js") }}" type="text/javascript"></script>

        {{-- Mask --}}
        <script src="{{ asset("assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js") }}" type="text/javascript"></script>
        <script src="{{ asset("assets/global/plugins/jquery.input-ip-address-control-1.0.min.js") }}" type="text/javascript"></script>

        {{-- Validation --}}
        <script src="{{ asset("assets/global/plugins/jquery-validation/js/jquery.validate.js") }}" type="text/javascript"></script>
        <script src="{{ asset("assets/global/plugins/jquery-validation/js/additional-methods.js") }}" type="text/javascript"></script>
@endsection

@section('after-plugins')
        {{-- Select --}}
        <script src="{{ asset("assets/pages/scripts/components-select2.min.js") }}" type="text/javascript"></script>

        {{-- Mask --}}
        <script src="{{ asset("assets/pages/scripts/form-input-mask.min.js") }}" type="text/javascript"></script>

        {{-- Validation --}}
        <script src="{{ asset("assets/pages/scripts/form-validation.js") }}" type="text/javascript"></script>
@endsection

@section('script')
    <script type="text/javascript">
        
    </script>
@endsection