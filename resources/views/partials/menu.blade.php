<ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
    <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
    <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
    <li class="sidebar-toggler-wrapper hide">
        <div class="sidebar-toggler">
            <span></span>
        </div>
    </li>
    <!-- END SIDEBAR TOGGLER BUTTON -->
    <li class="nav-item start" >
        <a href="{{ route('home') }}" class="nav-link nav-toggle">
            <i class="icon-home"></i>
            <span class="title">Home</span>
            <span class=""></span>
        </a>
    </li>
    <li class="heading">
        <h3 class="uppercase">Cadastros</h3>
    </li>
    <li class="nav-item {{strpos(Route::getFacadeRoot()->current()->uri(), 'ticket') !== false ? "active open" : ""}}">
        <a href="{{ route('contatos.index') }}" class="nav-link nav-toggle">
            <i class="fa fa-users"></i>
            <span class="title">Contatos</span>
            <span class="{{strpos(Route::getFacadeRoot()->current()->uri(), 'ticket') !== false ? "selected" : ""}}"></span>
        </a>
    </li>
</ul>