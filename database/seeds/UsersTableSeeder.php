<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Joao Silva',
            'email' => 'joao.silva@desk360.com.br',
            'password' => bcrypt('123321')
        ]);
    }
}
