<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;

abstract class BaseRepository
{
    /**
     * Modelo
     * @var Model
     */
    protected $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * Método retorna todos registros d obanco
     *
     * @return \Illuminate\Support\Collection
     */
    public function findAll()
    {
        return $this->model->all();
    }

    /**
     * Cria novo construtor do model
     *
     * @return <type>  ( description_of_the_return_value )
     */
    public function newQuery()
    {
        return new $this->model;
    }

    /**
     * Encontra model pelo id
     *
     * @param  int     $id  O identificador
     *
     * @return <type>  ( description_of_the_return_value )
     */
    public function findOrFail(int $id)
    {
        return $this->model->findOrFail($id);
    }

    /**
     * Método realiza update do recurso no banco de dados.
     *
     * @param  array $data
     * @param  int $value
     * @param  string $condition
     * @return int $id | model
     */
    public function update(array $data, int $value, string $condition = 'id')
    {
        $model = $this->model->where($condition, $value)->first();

        if (is_null($model)) {
            return false;
        }

        $model->fill($data);
        $model->save($data);

        return $model;
    }

    /**
     * Método armazena novo modelo no banco de dados.
     *
     * @param  array   $data  Os dados
     *
     * @return <type>  ( description_of_the_return_value )
     */
    public function store(array $data)
    {
        $model = $this->model->fill($data);
        $model->save($data);

        return $model;
    }

    /**
     * Delete o modelo
     *
     * @param  int     $id  O identificador
     *
     * @return <type>  ( description_of_the_return_value )
     */
    public function destroy(int $id)
    {
        $model = $this->findOrFail($id);
        return $model->delete();
    }
}
