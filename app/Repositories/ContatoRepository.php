<?php

namespace App\Repositories;

use App\Contato;
use App\Repositories\BaseRepository;
use Carbon\Carbon;

class ContatoRepository extends BaseRepository
{
    /**
     * Método construtor
     *
     * @param \App\Contato  $model  O modelo
     */
    public function __construct(Contato $model)
    {
        parent::__construct($model);
    }
}
