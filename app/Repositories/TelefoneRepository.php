<?php

namespace App\Repositories;

use App\Telefone;
use App\Repositories\BaseRepository;
use Carbon\Carbon;

class TelefoneRepository extends BaseRepository
{
    /**
     * Método construtor
     *
     * @param \App\Telefone  $model  O modelo
     */
    public function __construct(Telefone $model)
    {
        parent::__construct($model);
    }
}
