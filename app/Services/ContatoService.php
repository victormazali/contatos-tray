<?php

namespace App\Services;

use App\Mail\Welcome;
use App\Facades\DateHelper;
use App\Repositories\ContatoRepository;
use Illuminate\Support\Collection;

class ContatoService
{
    /**
     * Repositório de Contatos
     * @var \App\Repositories\ContatoRepository
     */
    protected $contatoRepository;

    /**
     * Método construtor
     *
     * @param \App\Repositories\ContatoRepository  $contatoRepository  O repositório de contatos
     */
    public function __construct(
        ContatoRepository $contatoRepository
    ) {
        $this->contatoRepository = $contatoRepository;
    }

    /**
     * Obtém todos os contatos.
     *
     * @return <type>  Todos os contatos.
     */
    public function getAllContacts()
    {
        return $this->contatoRepository->findAll();
    }

    /**
     * Inicia novo Contato
     *
     * @return <type>  ( description_of_the_return_value )
     */
    public function newContact()
    {
        return $this->contatoRepository->newQuery();
    }

    /**
     * Armazena um contato.
     *
     * @param  array   $data  Os dados
     *
     * @return <type>  ( description_of_the_return_value )
     */
    public function storeContact(array $data)
    {
        return $this->contatoRepository->store($data);
    }

    public function sendWelcomeEmail(int $id)
    {
        $user = $this->findOrFailContact($id);
        return \Mail::to($user)->send(new Welcome);
    }

    /**
     * Retorna um contato
     *
     * @param  int     $id  O identificador
     *
     * @return <type>  ( description_of_the_return_value )
     */
    public function findOrFailContact(int $id)
    {
        return $this->contatoRepository->findOrFail($id);
    }

    /**
     * Edita um contato
     *
     * @param  array   $data  Os dados
     * @param  int     $id    O identificador
     *
     * @return <type>  ( description_of_the_return_value )
     */
    public function updateContact(array $data, int $id)
    {
        return $this->contatoRepository->update($data, $id);
    }

    /**
     * Deleta um contato
     *
     * @param  int     $id  O identificador
     *
     * @return <type>  ( description_of_the_return_value )
     */
    public function destroyContact(int $id)
    {
        return $this->contatoRepository->destroy($id);
    }
}