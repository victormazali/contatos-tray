<?php

namespace App\Services;

use App\Mail\Welcome;
use App\Facades\DateHelper;
use App\Repositories\TelefoneRepository;
use Illuminate\Support\Collection;

class TelefoneService
{
    /**
     * Repositório de Telefones
     * @var \App\Repositories\TelefoneRepository
     */
    protected $telefoneRepository;

    /**
     * Método construtor
     *
     * @param \App\Repositories\TelefoneRepository  $telefoneRepository  O repositório de telefones
     */
    public function __construct(
        TelefoneRepository $telefoneRepository
    ) {
        $this->telefoneRepository = $telefoneRepository;
    }

    /**
     * Armazena um telefone.
     *
     * @param  array   $data  Os dados
     *
     * @return <type>  ( description_of_the_return_value )
     */
    public function storeTelefone(array $data)
    {
        return $this->telefoneRepository->store($data);
    }

    /**
     * Deleta um contato
     *
     * @param  int     $id  O identificador
     *
     * @return <type>  ( description_of_the_return_value )
     */
    public function destroyTelefone(int $id)
    {
        return $this->telefoneRepository->destroy($id);
    }
}