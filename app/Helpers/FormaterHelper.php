<?php

namespace App\Helpers;

class FormaterHelper {

	public $toFormat;

	public function __construct($format)
	{
		$this->toFormat = $format;
	}

	/**
	 * Data para formato database
	 * @param      string  $value  O valor 
	 * @return     string
	 */
	public function dateToDatabaseFormat(string $value)
	{
		if ( $this->toFormat == 'database' ) {

			if ($value !== null)
			{
				$date = explode('/', $value);
				return $date[2].'-'.$date[1].'-'.$date[0];
			}	
		} else { 
			// .... 
		}
	}
}