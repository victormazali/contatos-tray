<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Telefone extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'numero', 'contato_id'
    ];

    // belongsTo relations
    public function contato()
    {
        return $this->belongsTo('App\Contato');
    }
}
