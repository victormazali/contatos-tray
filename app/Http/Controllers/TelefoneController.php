<?php

namespace App\Http\Controllers;

use App\Services\TelefoneService;
use Illuminate\Http\Request;

class TelefoneController extends Controller
{
    /**
     * Servico de Telefones
     * @var \App\Services\TelefoneService
     */
    protected $telefoneService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(TelefoneService $telefoneService)
    {
        $this->telefoneService = $telefoneService;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) 
    {
        // Adiciona novo contato
        if ($telefone = $this->telefoneService->storeTelefone($request->toArray())) {

            flash('Telefone criado com sucesso.');

        } else {

            flash()->error('Não foi possível criar o telefone. Contate o administrador.');

        }

        return redirect()->route('contatos.telefones', $telefone->contato->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @internal param Request $request
     */
    public function destroy(int $id)
    {
        if ($this->telefoneService->destroyTelefone($id)) {
            flash()->success('Telefone deletado com sucesso');
        } else {
            flash()->success('Não foi possível deletar o telefone.');
        }

        return redirect()->back();
    }
}
