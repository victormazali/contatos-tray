<?php

namespace App\Http\Controllers;

use App\Contato;
use App\Services\ContatoService;
use Illuminate\Http\Request;

class ContatoController extends Controller
{
    /**
     * Servico de Contatos
     * @var \App\Services\ContatoService
     */
    protected $contatoService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ContatoService $contatoService)
    {
        $this->contatoService = $contatoService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = $this->contatoService->getAllContacts();

        return view('contatos.index', compact('result'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $contato = $this->contatoService->newContact();

        return view('contatos.new')->with(compact('contato'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) 
    {
        // Adiciona novo contato
        if ($contato = $this->contatoService->storeContact($request->toArray())) {

            $this->contatoService->sendWelcomeEmail($contato->id);

            flash('Contato criado com sucesso.');

        } else {

            flash()->error('Não foi possível criar o contato. Contate o administrador.');

        }

        return redirect()->route('contatos.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(int $id)
    {
        $contato = $this->contatoService->findOrFailContact($id);

        return view('contatos.edit', compact('contato'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {
        if ($this->contatoService->updateContact($request->toArray(), $id)) {
            flash()->success('Contato editado com sucesso.');
        } else {
            flash()->error('Não foi possível editar o contato.');
        }

        return redirect()->route('contatos.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @internal param Request $request
     */
    public function destroy(int $id)
    {
        if ($this->contatoService->destroyContact($id)) {
            flash()->success('Contato deletado com sucesso');
        } else {
            flash()->success('Não foi possível deletar o contato.');
        }

        return redirect()->route('contatos.index');
    }

    /**
     * Método administra os telefones do contato
     *
     * @param  int     $id  O identificador
     *
     * @return <type>  ( description_of_the_return_value )
     */
    public function administraTelefones(int $id)
    {
        $contato = $this->contatoService->findOrFailContact($id);

        return view('contatos.telefones', compact('contato'));
    }
}
