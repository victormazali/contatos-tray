<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contato extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'facebook_link', 'linkedin_link'
    ];

    // hasMany relations
    public function telefones()
    {
        return $this->hasMany('App\Telefone');
    }

}
